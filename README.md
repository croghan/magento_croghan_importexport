CLI/crontab importexport extension for Magento
================
Using existing Mage_ImportExport module, provides a shell script to run on demand and in crontab.


## Features

## Requirements

* PHP 5.3+

* Magento CE
  * 1.6
  * 1.7
  * 1.8
  * 1.9

* Magento EE
  * 1.11
  * 1.12
  * 1.13
  * 1.14

## Installation & Usage

## Support

**This extension is provided "as-is" for free.**

## Core Developers

  * [Michael Croghan](http://digitalkeg.com/)
