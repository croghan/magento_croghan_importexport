<?php
/**
 * Default helper
 *
 * Default helper for module
 *
 * @category    Croghan
 * @package     ImportExport
 * @author      Michael Croghan <mike@digitalkeg.com>
 */
class Croghan_ImportExport_Helper_Data extends Mage_Core_Helper_Abstract
{
	// global/model/<...config...>
	const XML_PATH_NODE_GROUP = 'global/models';
	// global/model/<model_resource_name>/croghan_importexport/<...config...>
	const XML_PATH_CONFIG_NODE_NAME = 'croghan_importexport';

	// global/model/<model_resource_name>/importexport/(import|export)/<...config...>
	const XML_PATH_IMPORT_CLASS_NODE_NAME = 'import';
	const XML_PATH_EXPORT_CLASS_NODE_NAME = 'export';

	// global/model/<model_resource_name>/importexport/(import|export)/class/<class name>
	const XML_PATH_CLASS_NODE_NAME = 'class';
	// global/model/<model_resource_name>/importexport/(import|export)/args/<optional parameters passed to class constructor>
	const XML_PATH_PARAMS_NODE_NAME = 'params';
	// global/model/<model_resource_name>/importexport/(import|export)/singleton/<true(default)|false>
	const XML_PATH_SINGLETON_NODE_NAME = 'singleton';

    /**
     * Get import model
     *
     * @param mixed $_modelMixed (obj | str)
     * @return Croghan_ImportExport_Model_Import_Abstract class (import model for model resource)
     */
	public function getImportModel ($_modelMixed)
	{
		// model resource name //
		$modelResourceName = self::_getModelResourceName ($_modelMixed);
		// model importexport config node; throws exception if not exist //
		$nodeConfig = self::_getModelImportExportConfig($modelResourceName);
		// model import config node //
		$nodeImportConfig = $nodeConfig->{self::XML_PATH_IMPORT_CLASS_NODE_NAME};

		// import not defined; complain //
		if( ! $nodeImportConfig){
			Mage::throwException(sprintf("model resource: %s has a croghan_importexport configuration, but import not defined", $modelResourceName));
		}

		// import class not defined; complain //
		if( ! $nodeImportConfig->{self::XML_PATH_CLASS_NODE_NAME}){
			Mage::throwException(sprintf("model resource: %s import class not defined", $modelResourceName));	
		}

		// create as singleton or not //
		$bSingleton = $nodeImportConfig->{self::XML_PATH_SINGLETON_NODE_NAME} ? true : false;
		// get model! //
		$model = self::_getModel($nodeImportConfig->{self::XML_PATH_CLASS_NODE_NAME}, $nodeImportConfig->{self::XML_PATH_PARAMS_NODE_NAME}, $bSingleton);

		return $model;
	}

    /**
     * Get export model
     *
     * @param mixed $_modelMixed (obj | str)
     * @return Croghan_ImportExport_Model_Export_Abstract class (export model for model resource)
     */
	public function getExportModel ($_modelMixed)
	{
		$exportModel = null;

		// model resource name //
		$modelResourceName = self::_getModelResourceName ($_modelMixed);
		// model importexport config node; throws exception if not exist //
		$nodeConfig = self::_getModelImportExportConfig($modelResourceName);
		// model export config node //
		$nodeExportConfig = $nodeConfig->{self::XML_PATH_IMPORT_CLASS_NODE_NAME};

		// export not defined; complain //
		if( ! $nodeExportConfig){
			Mage::throwException(sprintf("model resource: %s has a croghan_importexport configuration, but export not defined", $modelResourceName));
		}

		// export class not defined; complain //
		if( ! $nodeExportConfig->{self::XML_PATH_CLASS_NODE_NAME}){
			Mage::throwException(sprintf("model resource: %s export class not defined", $modelResourceName));	
		}

		// create as singleton or not //
		$bSingleton = $nodeExportConfig->{self::XML_PATH_SINGLETON_NODE_NAME} ? true : false;
		// get model! //
		$model = self::_getModel($nodeExportConfig->{self::XML_PATH_CLASS_NODE_NAME}, $nodeExportConfig->{self::XML_PATH_PARAMS_NODE_NAME}, $bSingleton);

		return $model;
	}

    /**
     * Get model
     *
     * @param string $_modelName
     * @param Mage_Core_Model_Config $_params (arguments to pass model constructor)
     * @param boolean $_singleton (create as singleton or not)
     * @return Mage_Core_Model_Config (model resource importexport config node)
     */
	protected function _getModel ($_modelName, $_params, $_singleton)
	{
		$params = null != $_params ? $_params->asArray() : [];
		$model = true == $_singleton ? Mage::getSingleton($_modelName, $params) : Mage::getModel($_modelName, $params);

		// model not defined; complain //
		if( ! $model){
			Mage::throwException(sprintf("model: %s does not exist", $_modelName));
		}

		return $model;
	}

    /**
     * Get model resource name
     *
     * @param mixed $_modelMixed (obj | str)
     * @return str
     */
	protected function _getModelResourceName ($_modelMixed)
	{
		$resourceName = $_modelMixed;

		// get resource name from model //
		if (is_object ($_modelMixed)){
			// not model abstract; complain //
			if ( ! $_modelMixed instanceof Mage_Core_Model_Abstract){
				Mage::throwException(sprintf("model resource: object passed not an instance of Mage_Core_Model_Abstract"));
			}
			$resourceName = $_modelMixed->getResourceName();
		}

		return $resourceName;
	}

    /**
     * Get model config node
     *
     * @param str $_modelResourceName
     * @return Mage_Core_Model_Config (model resource importexport config node)
     */
	protected function _getModelImportExportConfig ($_modelResourceName)
	{
		// importexport node path for model //
		$nodePath = sprintf("%s/%s/%s", self::XML_PATH_NODE_GROUP, $_modelResourceName, self::XML_PATH_CONFIG_NODE_NAME);
		// config node //
		$nodeConfig = Mage::app()->getConfig()->getNode($nodePath);

		// not defined; complain //
		if( ! $nodeConfig){
			Mage::throwException(sprintf("model resource: %s has no defined croghan_importexport configuration", $_modelResourceName));
		}

		return $nodeConfig;
	}
}